import { WeatherActionTypes, WEATHER_REQUEST, WEATHER_SUCCESS, WEATHER_FAILURE } from '../types/weatherTypes'

export const weatherRequest = (): WeatherActionTypes => {
  return {
    type: WEATHER_REQUEST,
  }
}

export const weatherSuccess = (data: any): WeatherActionTypes => {
  return {
    type: WEATHER_SUCCESS,
    payload: data
  }
}

export const weatherFailure = (error: any): WeatherActionTypes => {
  return {
    type: WEATHER_FAILURE,
    error: error
  }
}

// import * as constants from '../constant'

// export interface IWeatherRequest {
//   type: constants.WEATHER_REQUEST
// }

// export interface IWeatherSuccess {
//   type: constants.WEATHER_SUCCESS
//   data: {}
// }

// export interface IWeatherFailure {
//   type: constants.WEATHER_FAILURE
//   error: {}
// }

// export const weatherRequest = (): IWeatherRequest => ({
//   type: constants.WEATHER_REQUEST,
// })

// export const weatherSuccess = (data: any): IWeatherSuccess => ({
//   type: constants.WEATHER_SUCCESS,
//   data: data
// })

// export const weatherFailure = (err: any): IWeatherFailure => ({
//   type: constants.WEATHER_FAILURE,
//   error: err
// })

// export type Locality = IWeatherRequest | IWeatherSuccess | IWeatherFailure