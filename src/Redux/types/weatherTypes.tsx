export const WEATHER_REQUEST = 'WEATHER_REQUEST'
export const WEATHER_SUCCESS = 'WEATHER_SUCCESS'
export const WEATHER_FAILURE = 'WEATHER_FAILURE'

export interface Data {
  data?: any
}

export interface WeatherState {
  data?: Data
  fetching: boolean
  success: boolean
  error?: any
}

interface IWeatherRequest {
  type: typeof WEATHER_REQUEST
}

interface IWeatherSuccess {
  type: typeof WEATHER_SUCCESS
  payload: Data
}

interface IWeatherFailure {
  type: typeof WEATHER_FAILURE
  error: any
}

export type WeatherActionTypes = IWeatherRequest | IWeatherSuccess | IWeatherFailure