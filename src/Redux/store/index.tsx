import { combineReducers } from "redux";
import { weatherReducer } from "../reducer/weatherReducer";

const rootReducer = combineReducers({
  weatherState: weatherReducer
})

export type RootState = ReturnType<typeof rootReducer>