import { WeatherState, WeatherActionTypes, WEATHER_REQUEST, WEATHER_SUCCESS, WEATHER_FAILURE } from "../types/weatherTypes";

const INITIAL_STATE: WeatherState = {
  data: {},
  fetching: false,
  success: false,
  error: null
}

export function weatherReducer(state = INITIAL_STATE, action: WeatherActionTypes): WeatherState {
  switch(action.type) {
    case WEATHER_REQUEST:
      return {
        ...state,
        fetching: true,
        success: false,
        error: false
      }
    case WEATHER_SUCCESS:
      return {
        ...state,
        fetching: false,
        success: true, 
        data: action.payload
      }
    case WEATHER_FAILURE:
      return {
        ...state,
        fetching: false,
        success: false,
        error: action.error
      }

    default: return state
  }
}

// import { Locality } from '../action/weatherAction'
// import { StoreState } from '../../types/index'
// import * as constants from '../constant'

// export function locality(state: StoreState, action: Locality): StoreState {
  
//   switch(action.type) {
//     case constants.WEATHER_REQUEST:
//       return {
//         ...state,
//         fetching: true,
//         success: false,
//         error: false
//       }
//     case constants.WEATHER_SUCCESS:
//       return {
//         ...state,
//         fetching: false,
//         success: true,
//         data: action.data
//       }
//     case constants.WEATHER_FAILURE:
//       return {
//         ...state,
//         fetching: false,
//         success: false,
//         error: action.error
//       }
//   }
  
//   return state
// }