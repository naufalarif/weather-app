export default {
  API_KEY: '0665a006c1ea2e6608ce37fa755e8263',
  baseURL: 'https://api.openweathermap.org/',
  imageURL: 'http://openweathermap.org/img/wn/',
  oneCall: 'data/2.5/onecall?lat=33.441792&lon=-94.037689&exclude=daily&appid=',
  currentWeather: 'data/2.5/weather?q=',
}