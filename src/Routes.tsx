import React, { Component } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import HomePage from './Container/HomePage'

class Routes extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path='/' component={HomePage} />
        </Switch>
      </BrowserRouter>
    )
  }
}

export default Routes
