import React, { Component } from 'react'
import { create } from 'apisauce'
import config from '../config'
import moment from 'moment'
import _ from 'lodash'
import CloudDay from '../assets/cloud.jpg'
import ClearDay from '../assets/clearsky-d.jpg'
import RainDay from '../assets/rain-d.jpg'
import ThunderDay from '../assets/thunderstorm-d.jpg'
import DrizzleDay from '../assets/drizzle-d.jpeg'
import MistDay from '../assets/mist-d.jpg'
import Search from '../assets/search.png'
import { Row, Col, Divider } from 'antd'

const Api = create({
  baseURL: config.baseURL
})

interface Props {

}

interface State {
  data: any,
  city: String,
  query: String,
  submit: boolean
}

class HomePage extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      data: {},
      city: '',
      query: '',
      submit: false
    }
  }

  componentDidMount() {
    this.fetchData()
  }

  componentDidUpdate() {
    if(this.state.submit) {
      this.fetchData()
    }
  }

  async fetchData() {
    const cityName = this.state.city || 'Bekasi'
    Api
      .get(`${config.currentWeather}${cityName}&appid=${config.API_KEY}`)
      .then(res => this.setState({ data: res.data }))
  }

  handleChange = (e: any) => {
    this.setState({
      query: e.target.value
    })
  }

  handleSubmit = () => {
    this.setState({
      city: this.state.query
    })
  }

  public render() {
    const { data }: any = this.state
    const main = !_.isEmpty(data.main) ? data.main : {}
    const sys = !_.isEmpty(data.sys) ? data.sys : {}
    const wind = !_.isEmpty(data.wind) ? data.wind : {}
    const clouds = !_.isEmpty(data.clouds) ? data.clouds : {}
    const weather:any[] = !_.isEmpty(data.weather) ? data.weather : []
    
    // Weather
    const currWeather = weather.map(data => data.main)
    const descWeather = weather.map(data => data.description)
    const iconWeather = weather.map(data => data.icon)
    
    // Temp
    const calculated = main.temp - 273.15
    const temp = calculated.toFixed(1)
    
    // Sys
    const country = _.isEqual(sys.country, "ID") ? "Indonesia" : null
    const epochSunrise = sys.sunrise
    const epochSunset = sys.sunset
    
    // Date
    const epochDate = data.dt
    const date = moment(epochDate * 1000).format("HH:mm - dddd, DD MMM 'YY") 
    const sunrise = moment(epochSunrise * 1000).format("HH:mm") 
    const sunset = moment(epochSunset * 1000).format("HH:mm") 
    const heightRes = window.innerHeight
    
    const bg:any = 
        _.isEqual(currWeather.join(), "Clear") ? ClearDay 
        : _.isEqual(currWeather.join(), "Clouds") ? CloudDay 
        : _.isEqual(currWeather.join(), "Rain") ? RainDay 
        : _.isEqual(currWeather.join(), "Drizzle") ? DrizzleDay 
        : _.isEqual(currWeather.join(), "Thunderstorm") ? ThunderDay 
        : _.isEqual(currWeather.join(), "Mist") ? MistDay 
        : null

    console.log(bg)
    return (
      <div>
        <img 
          className="card-img overflow-hidden" 
          src={bg} 
          width='100%'
          height={window.innerHeight}
          alt="background"
        />
        <div className="card-img-overlay">
          <Row>
            <Col span={16}>
              <div className="p-5">
                <h2>{country}</h2>
              </div>
              <div className="main-content fixed-bottom p-5" id="fixed-width">
                <Row>
                  <Col style={{ marginRight: '10px' }}>
                    <div>
                      <p style={{ fontSize: '86px', marginBottom: '0' }}>{temp}°</p>
                    </div>
                  </Col>
                  <Col>
                    <div style={{ marginTop: '25px' }}>
                      <p style={{ fontSize: '44px', marginBottom: '-10px' }}>{data.name}</p>
                      <p style={{ fontSize: '14px', marginBottom: '0' }}>{date}</p>  
                    </div>
                  </Col>
                  <Col className="text-center">
                    <div style={{ marginTop: '0', marginBottom: '-15px' }}>
                      <img 
                        src={`${config.imageURL}${iconWeather}@2x.png`}
                        width='100%'
                        alt='icon'
                      />
                    </div>
                    <div >
                      <p style={{ marginBottom: '0', marginTop: '0' }}>{currWeather}</p>
                    </div>
                  </Col>
                </Row>
              </div>
            </Col>
            <Col span={8} className="side-container">
              <div className="side-bg" style={{ height: `${heightRes}px` }}></div>
              <div className="card-img-overlay">
                <div className="side-content">
                  <div>
                    <Row>
                      <Col span={20}>
                        <div className="pl-5 pt-5 input-container">
                          <input
                            className="input-form" 
                            placeholder="Another Location"
                            onChange={this.handleChange}
                          />
                        </div>
                      </Col>
                      <Col span={4}>
                        <div className="search-container" onClick={this.handleSubmit}>
                          <img 
                            src={Search}
                            width="60%"
                            alt='search'
                          />
                        </div>
                      </Col>
                    </Row>
                  </div>
                  <div className='p-5'>
                    <Divider />
                    <div className="pb-4">
                      <h3>Weather Detail</h3>
                    </div>
                    <Row>
                      <Col span={12}>
                        <div>
                          <p>Cloudy ({descWeather})</p>
                        </div>
                      </Col>
                      <Col span={12} className="text-right">
                        <div>
                          <p>{clouds.all}%</p>
                        </div>
                      </Col>
                    </Row>
                    <Row>
                      <Col span={12}>
                        <div>
                          <p>Humidity</p>
                        </div>
                      </Col>
                      <Col span={12} className="text-right">
                        <div>
                          <p>{main.humidity}%</p>
                        </div>
                      </Col>
                    </Row>
                    <Row>
                      <Col span={12}>
                        <div>
                          <p>Wind</p>
                        </div>
                      </Col>
                      <Col span={12} className="text-right">
                        <div>
                          <p>{wind.speed}m/s</p>
                        </div>
                      </Col>
                    </Row>
                    <Row>
                      <Col span={12}>
                        <div>
                          <p>Sunrise</p>
                        </div>
                      </Col>
                      <Col span={12} className="text-right">
                        <div>
                          <p>{sunrise}</p>
                        </div>
                      </Col>
                    </Row>
                    <Row>
                      <Col span={12}>
                        <div>
                          <p>Sunset</p>
                        </div>
                      </Col>
                      <Col span={12} className="text-right">
                        <div>
                          <p>{sunset}</p>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}

export default HomePage
